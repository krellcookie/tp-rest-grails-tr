package fr.mbds.tp

class Message {

    String messageContent

    Date dateCreated

    User author

    boolean isDeleted

    static constraints = {
        messageContent nullable: false, blank: false
        author nullable: true
    }
}
