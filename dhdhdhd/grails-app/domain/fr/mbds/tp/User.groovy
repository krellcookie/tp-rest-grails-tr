package fr.mbds.tp

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String username
    String password
    String firstName
    String lastName
    String email
    String tel
    Date dob

    Date dateCreated
    Date lastUpdated

    boolean enabled = true
    boolean isDeleted

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    /*static hasMany = [magSent: Message, magReceived: Message]
    static mappedBy = [magSent: "author", magReceived: "receiver"]*/

    static constraints = {
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        email nullable: false, blank: false, unique: true
        dob nullable: true
        tel nullable: true
        firstName nullable: false, blank: false
        lastName nullable: false, blank: false
        isDeleted nullable : false, blank: false
    }

    static mapping = {
	    password column: '`password`'
    }
}
