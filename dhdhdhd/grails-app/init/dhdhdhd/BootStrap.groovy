package dhdhdhd

import fr.mbds.tp.Message
import fr.mbds.tp.Role
import fr.mbds.tp.User
import fr.mbds.tp.UserMessage
import fr.mbds.tp.UserRole

class BootStrap {

    def init = { servletContext ->
        def userAdmin = new User (username: "Admin", password: "secret", firstName: "admin", lastName: "admin", email: "email_admin", isDeleted: false).save(flush: true, failOnError: true)
        def roleAdmin = new Role (authority: "ROLE_ADMIN", isDeleted: false).save(flush: true, failOnErro: true)
        def roleUser = new Role (authority: "ROLE_USER", isDeleted: false).save(flush: true, failOnErro: true)

        UserRole.create(userAdmin, roleAdmin, true)
        (1..50).each {
            def userInstance = new User(username: "username-$it", password: "password", firstName: "first", lastName: "last", email: "email-$it", isDeleted: false).save(flush: true, failOnError: true)
            UserRole.create(userInstance, roleUser,true)
            new Message(messageContent: "lala", author: userInstance, isDeleted: false).save(flush: true)
        }
        Message.list().each{
            Message messageInstance ->
            User.list().each{
                User userInstance ->
                    UserMessage.create(userInstance, messageInstance, true)
            }
        }

    }
    def destroy = {
    }
}
