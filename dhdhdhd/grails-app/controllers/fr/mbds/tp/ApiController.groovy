package fr.mbds.tp

import grails.converters.JSON
import grails.converters.XML
import org.apache.catalina.servlet4preview.http.HttpServletRequest
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletRequest
class ApiController {

    def index() {
        render "ok"
    }

    def reponseFormat(Object instance, request) {
        switch (request.getHeader("Accept")) {
            case "text/xml":
                render instance as XML
                break;
            case "text/json":
                render instance as JSON
                break;
        }
    }

    def reponseFormatList(List list, HttpServletRequest request) {
        switch (request.getHeader("Accept")) {
            case "text/xml":
                render list as XML
                break;
            case "text/json":
                render list as JSON
                break;
        }
    }

    def message() {
        switch (request.getMethod()) {
            case "GET":
                if (params.id) //On doit retourner une instance de message
                {
                    def messageInstance = Message.get(params.id)
                    if (messageInstance) {
                        reponseFormat(messageInstance, request)
                        render (status: 200, text: "Voici le message ${messageInstance.id}")
                    }
                    else
                        render (status: 404, text: "Le message est introuvable")
                }
                else
                    forward action: "messages"
                break;

            case "POST":
                forward action: "messages"
                break;

            case "PUT":
                def messageInstance = params.id ? Message.get(params.id) : null
                if (messageInstance) {
                    if (params.authorid) {
                        def authorInstance = User.get((Long) params.get("authorid"))
                        if (authorInstance) {
                            messageInstance.author = authorInstance
                        }
                        else
                            render(status: 404, text: "L'auteur ${authorInstance.id} est introuvable")
                    }

                    if (params.messageContent) {
                        messageInstance.messageContent = params.messageContent
                    }
                    if (messageInstance.save(flush: true)) {
                        render(text: "Mise à jour effectuée pour le message ${messageInstance.id}")
                    }
                    else
                        render(status: 400, text: "Echec de la MAJ du message ${messageInstance.id}")
                }
                else
                    render(status: 404, text: "le message est introuvable")
                break;

            case "DELETE":
                def messageInstance = params.id ? Message.get(params.id) : null
                if (messageInstance) {
                    def userMessages = UserMessage.findAllByMessage(messageInstance)
                    userMessages.each {
                        UserMessage userMessage ->
                            userMessage.delete(flush: true)
                    }
                    messageInstance.delete(flush: true)
                    render(status: 200, text: "Message ${messageInstance.id} effacé")
                }
                else
                    render(status: 404, text: "Message ${messageInstance.id} introuvable")
                break;
            default:
                render (status: 405, text: "La suppression du message est impossible")
                break
        }
    }


    def messages() {
        switch (request.getMethod()) {
            case "GET":
                reponseFormatList(Message.list(), request)
                break;
            case "POST":
                //Vérifier l'auteur
                //Créer le message
                def authorInstance = params.authorid ? User.get(params.authorid) : null
                def messageInstance
                if (authorInstance) {
                    messageInstance = new Message(author: authorInstance, messageContent: params.messageContent)
                    if (messageInstance.save(flush: true)) {
                        render(status: 201, text: "le message ${authorInstance.id} a été créé")
                    }
                    else
                        render (status: 400, text: "Le message est impossible à créer")
                }
                else
                    render (status: 404, text: "L'utilisateur est introuvable")
                break;
        }
    }

    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (params.id)
                {
                    def userInstance = User.get(params.id)
                    if (userInstance) {
                        reponseFormat(userInstance, request)
                        render (status: 200, text: "Voici l'utilisateur ${userInstance.id}")
                    }
                    else
                        render (status: 404, text: "Impossible de trouver l'utilisateur")
                }
                else
                    forward action: "users"
                break;

            case "POST":
                forward action: "users"
                break;

            case "PUT":
                def userInstance = params.id ? User.get(params.id) : null
                if (userInstance) {
                    if (userInstance) {
                        if (params.username) {
                            userInstance.username = params.username
                        }
                        if (params.password) {
                            userInstance.password = params.password
                        }
                        if (params.email) {
                            userInstance.email = params.email
                        }
                        if (params.dob) {
                            userInstance.dob = params.dob
                        }
                        if (params.tel) {
                            userInstance.tel = params.tel
                        }
                        if (params.firstName) {
                            userInstance.firstName = params.firstName
                        }
                        if (params.lastName) {
                            userInstance.lastName = params.lastName
                        }
                        userInstance.save(flush: true)
                    }
                    if (userInstance.save(flush: true)) {
                        render(text: "Mise à jour effectuée pour l'utilisateur ${userInstance.id}")
                    }
                    else
                        render(status: 400, text: "Echec de la MAJ ${userInstance.id}")
                }
                else
                    render(status: 404, text: "L'utilisateur demandé est introuvable")
                break;

            case "DELETE":
                def userInstance = params.id ? User.get(params.id) : null
                if (userInstance) {
                    userInstance.isDeleted = false
                    userInstance.delete(flush: true)
                    render(status: 201, text: "Utilisateur effacé")
                }
                else
                    render(status: 404, text: "Utilisateur introuvable")
                break;
            default:
                render (status: 405, text: "La suppression de l'utilisateur est impossible")
                break
        }
    }

    def users() {
        switch (request.getMethod()) {
            case "GET":
                reponseFormatList(User.list(), request)
                break;
            case "POST":
                def userInstance = new User(password: params.password, username: params.username, email: params.email, dob: params.dob, tel: params.tel, firstName: params.firstName, lastName: params.lastName, isDeleted: params.isDeleted)
                if (userInstance.save(flush: true)) {
                    render(status: 201, text: "Nouvel utilisateur ${userInstance.id} créé")
                }
                if (response.status != 201)
                    render(status: 400, text: "Création de l'utilisateur impossible")
                break;
        }
    }

    def messageToUser() {
        switch (request.getMethod()) {
            case "POST":
                if (params.get("userid")) {
                    def userInstance = User.get(params.userid)
                    if (userInstance) {
                        def messageInstance;
                        if (params.get("messageid")) {
                            messageInstance = Message.get(params.messageid)
                        }
                        else {
                            def authorInstance = params.get("authorid") ? User.get(params.authorid) : null
                            if (authorInstance)
                                messageInstance = new Message(author: authorInstance, messageContent: params.messageContent).save(flush: true)
                        }
                        if(messageInstance) {
                            def userMessageInstance =  new UserMessage(message: messageInstance, user: userInstance)
                            if(userMessageInstance.save(flush: true))
                                render(status: 201, text: "Envoi du message ${messageInstance.id} au destinataire ${userInstance.id} réussi")
                        }
                        else {
                            render(status: 400, text: "Message non récupéré")
                        }
                    }
                    else {
                        render(status: 404, text: "L'utilisateur est introuvable")
                    }
                }

                if (response.status != 201){
                    render(status: 400, text: "Message non attribué à un utilisateur")
                }

            default:
                response.status = 405
                break
        }
    }

    def messageToGroup() {
        switch (request.getMethod()) {
            case "POST":
                if (params.get("groupid")) {
                    def roleInstance = Role.get(params.groupid)
                    def userRoleList = UserRole.findAllByRole(roleInstance)
                    def userList = userRoleList.collect{ it.user }
                    if (roleInstance) {
                        def messageInstance;
                        if (params.get("messageid")) {
                            messageInstance = Message.get(params.messageid)
                        }
                        else {
                            def authorInstance = params.get("authorid") ? User.get(params.authorid) : null
                            if (authorInstance)
                                messageInstance = new Message(author: authorInstance, messageContent: params.messageContent).save(flush: true)
                        }
                        if(messageInstance) {
                            for (user in userList) {
                                def userMessageInstance =  new UserMessage(message: messageInstance, user: user)
                                if(userMessageInstance.save(flush: true))
                                    render(status: 201, text: "Envoi du message ${messageInstance.id} à l'utilisateur ${user.id} réussi")
                            }
                        }
                        else {
                            render(status: 400, text: "Message non récupéré")
                        }
                    }
                    else {
                        render(status: 404, text: "Le groupe est introuvable")
                    }
                }

                if (response.status != 201) {
                    render(status: 400, text: "Message non attribué à un utilisateur")
                }

            default:
                response.status = 405
                break
        }
    }
}